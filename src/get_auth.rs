// Import necessary types from crates.
use exonum::crypto::{verify, HexValue, PublicKey, Signature};
use exonum::api::ApiError;
use iron::prelude::*;
use iron::url::Url;
use mount::OriginalUrl;
use chrono::DateTime;
use time::{self};

fn authenticate(key: PublicKey, req: &mut Request) -> IronResult<bool> {
    let verification = || -> Result<bool, &str> {
        let headers = &req.headers;

        let extensions = &req.extensions;
        let unparsed_url = extensions
            .get::<OriginalUrl>()
            .ok_or("Extensions error")?
            .clone();
        let url: Url = unparsed_url.into();
        let url_str = url.as_str().as_bytes();

        let method = b"GET";

        // Date check
        let date = headers.get_raw("x-date").ok_or("Date error")?[0].as_slice();
        let d = DateTime::parse_from_rfc3339(&String::from_utf8_lossy(date)).unwrap();
        let timestamp = d.timestamp();
        let now = time::now_utc().to_timespec().sec;
        if (now - timestamp) >= 30 {
            return Err("Timestamp expired");
        }
        // ----------

        // Signature parse
        let signature = headers.get_raw("x-auth").ok_or("Headers error")?[0].as_slice();
        let signature_hex = Signature::from_hex(String::from_utf8_lossy(signature))
            .ok()
            .ok_or("Signature error")?;
        // ---------------

        // Making data chunk for verification
        let mut data = Vec::new();
        data.extend_from_slice(url_str);
        data.extend_from_slice(method);
        data.extend_from_slice(date);
        // ----------------------------------

        Ok(verify(&signature_hex, data.as_slice(), &key))
    };

    match verification() {
        Ok(verified) => Ok(verified),
        Err(e) => Err(ApiError::IncorrectRequest(e.into()))?,
    }
}

pub fn authenticate_request<F>(
    key: PublicKey,
    logic: F,
) -> impl Fn(&mut Request) -> IronResult<Response>
where
    F: Fn(&mut Request) -> IronResult<Response>,
{
    move |req| match authenticate(key, req) {
        Ok(verified) => if verified {
            println!("Get request authentication passed");
            logic(req)
        } else {
            Err(ApiError::IncorrectRequest("Authentication failure".into()))?
        },
        Err(e) => Err(ApiError::IncorrectRequest(format!("{}", e).into()))?,
    }
}